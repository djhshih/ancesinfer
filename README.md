
## Installation

From within the project directory, using R with `devtools` installed, do
```
> document()
> install()
```

## Citation

Shih et al. Genomic characterization of human brain metastases identifies drivers of metastatic lung adenocarcinoma.
Nat Genet. 2020 Apr;52(4):371-377. doi: 10.1038/s41588-020-0592-7.

