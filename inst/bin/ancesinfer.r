#!/usr/bin/env Rscript
library(data.table, quietly = TRUE);
library(argparser, quietly=TRUE);
library(ancesinfer, quietly=TRUE);

pr <- arg_parser("Ancestry inference from MuTect results");
pr <- add_argument(pr, "input", help="SNP file from Mutect callstats, Mutect2 VCF or GATK allelicCounts.tsv file");
pr <- add_argument(pr, "output", help="output file");
pr <- add_argument(pr, "--input.type", default = "mutect2", help="input file generated from which program: mutect/mutect2/gatk"); 
pr <- add_argument(pr, "--genome", default = "hg38", help="genome build: hg19/hg38");


argv <- parse_args(pr);

if(argv$genome == "hg19"){
	data(model_nb, package = "ancesinfer");
}
if(argv$genome == "hg38"){
	data(model_nb_hg38, package = "ancesinfer");
}

if(argv$input.type == "mutect"){
	input <- read.table(argv$input, sep="\t", header=TRUE, stringsAsFactors=FALSE);
	germline <- germline_genotype_from_mutect(input);
}

if(argv$input.type == "mutect2"){
	input <- fread(argv$input, sep = "\t", header = TRUE, skip = '#CHROM')
	colnames(input)[1] <- "CHROM"
	germline <- germline_genotype_from_mutect2(input);
}

if(argv$input.type == "gatk"){
	input <- read.table(argv$input, sep = "\t", header = TRUE, comment.char = "@")
	germline <- germline_genotype_from_gatkcnv(input);
}

#data(model_nb_hg38, package = "ancesinfer");
#input <- read.table(argv$input, sep = "\t", header = TRUE, comment.char = "@")
#germline <- germline_genotype_from_gatkcnv(input);

gt <- with(germline, make_genotype(chrom, pos, ref, alt, gt, model = model_nb));

y <- predict(model_nb, data=gt, type="log_posterior");

d <- data.frame(
	population = names(y),
	log_posterior = y,
	row.names=NULL
);

write.table(d, argv$output, sep="\t", quote=FALSE, row.names=FALSE);
